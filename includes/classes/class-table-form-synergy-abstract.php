<?php

require_once ABSPATH . 'wp-includes/PHPMailer/PHPMailer.php';
require_once ABSPATH . 'wp-includes/PHPMailer/SMTP.php';
require_once ABSPATH . 'wp-includes/PHPMailer/Exception.php';

use PHPMailer\PHPMailer\PHPMailer;
abstract class Table_Form_Synergy_Abstract extends PHPMailer{

	public function get_smtp_mail_synergy() : PHPMailer
	{
		return new PHPMailer();
	}

	/**
	 * @throws \PHPMailer\PHPMailer\Exception
	 */
	public function send_mail_synergy( PHPMailer $mailer, array $args ): string
	{

		$mailer->setFrom($args['email'],$args['name']);
		$mailer->addAddress('ciao@gmail.com','Lello');
		$mailer->Subject = 'Email di test';
		$mailer->isHTML();
		$mailContent = "<h1>Email dal form Synergy</h1><p>".$args['body']."</p>";
		$mailer->Body = $mailContent;

		if($mailer->send()){
			return "Messaggio inviato correttamente.";
		}else{
			return 'Errore' . $mailer->ErrorInfo;
		}
	}
}