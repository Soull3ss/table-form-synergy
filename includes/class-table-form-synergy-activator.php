<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    table_form_synergy
 * @subpackage table_form_synergy/includes
 */


/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    table_form_synergy
 * @subpackage table_form_synergy/includes
 * @author     Your Name <email@example.com>
 */
class Table_Form_Synergy_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate(): void
	{
        self::create_table_db();
	}

    public static function create_table_db() : void
    {
        /** @var wpdb */
        global $wpdb;

        // create table for storing submissions
        $table = $wpdb->prefix . 'table_form_synergy';
        $wpdb->query(
            "CREATE TABLE IF NOT EXISTS {$table}(
				        `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
				        `name` VARCHAR(255) NOT NULL,                        
				        `surname` VARCHAR(255) NOT NULL,
    					`email` VARCHAR(255) NOT NULL,
                        `date_of_birth` VARCHAR(255) NOT NULL, 
                        `born_in` VARCHAR (255) NOT NULL,                        
				        `submitted_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
				) ENGINE=INNODB CHARACTER SET={$wpdb->charset};"
        );
    }

}


