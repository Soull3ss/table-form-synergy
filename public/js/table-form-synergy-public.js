(function( $ ) {
	'use strict';
	$(document).ready(function() {
		$('#button-sub').on('click',function(event){
			event.preventDefault();

			let form = $('#form-information')[0];

			let varform = new FormData(form);

			varform.append("action", "store_form_information");
			varform.append("nonce",my_ajax_handler.nonce);

			$.ajax({
				type: "POST",
				url: my_ajax_handler.ajaxurl,
				dataType: "JSON",
				data: varform,
				processData: false,
				contentType: false,
				cache: false,
				crossDomain:true,
				success: function(response){

					console.log("succes:"+response.data);

					if(response.success === true){

						toastr.options = {
							"closeButton": true,
							"debug": false,
							"newestOnTop": false,
							"progressBar": false,
							"positionClass": "toast-bottom-center",
							"preventDuplicates": false,
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}

						toastr.success("Messaggio inviato.", response.data.message);
						$('#form-information').trigger("reset");
					}else{
						console.error(response.error);
						toastr.error("Qualcosa non ha funzionato", "Riprova")
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(thrownError);
				}

			});
		});
	});


})( jQuery );
