<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Table_Form_Synergy
 * @subpackage Table_Form_Synergy/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="container">
    <div class="row">
        <form id="form-information">
            <div class="mb-3">
                <label for="name" class="form-label">Nome</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="mb-3">
                <label for="surname" class="form-label">Cognome</label>
                <input type="text" class="form-control" id="surname" name="surname">
            </div>
            <div class="mb-3">
                <label for="surname" class="form-label">Email</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>
            <div class="mb-3">
                <label for="birth" class="form-label">Data di nascita</label>
                <input type="date" class="form-control" id="birth" name="birth">
            </div>
            <div class="mb-3">
                <label for="born" class="form-label">Nato a</label>
                <input type="text" class="form-control" id="born" name="born">
            </div>
            <button class="btn btn-outline-secondary" id="button-sub" type="submit">Invia il form</button>
        </form>

    </div>
</div>