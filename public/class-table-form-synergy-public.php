<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Table_Form_Synergy
 * @subpackage Table_Form_Synergy/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Table_Form_Synergy
 * @subpackage Table_Form_Synergy/public
 * @author     Your Name <email@example.com>
 */

require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/classes/class-table-form-synergy-abstract.php';
use \PHPMailer\PHPMailer\PHPMailer;

class Table_Form_Synergy_Public extends Table_Form_Synergy_Abstract{

	public PHPMailer $mailer;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $Table_Form_Synergy    The ID of this plugin.
	 */
	private string $table_form_synergy;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private string $version;
	private string $EMAIL_FROM = 'test@test.com';

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param $table_form_synergy
	 * @param string $version The version of this plugin.
	 *
	 * @since    1.0.0
	 */
	public function __construct($table_form_synergy, string $version ) {
		parent::__construct();

		$this->table_form_synergy = $table_form_synergy;
		$this->version = $version;

		//$this->mailer = $this->get_smtp_mail_synergy();
		//$this->set_option_mail_synergy();
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() : void
    {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Table_Form_Synergy_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Table_Form_Synergy_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->table_form_synergy, plugin_dir_url(__FILE__) . 'css/table-form-synergy-public.css', array(), $this->version, 'all' );
        wp_enqueue_style( $this->table_form_synergy. '-bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css', array(), $this->version, 'all' );
        wp_enqueue_style( $this->table_form_synergy. '-toastr-css', 'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() : void {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Table_Form_Synergy_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Table_Form_Synergy_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		$vars = array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'nonce'   => wp_create_nonce( 'my-ajax-handler-nonce' )
		);
		wp_enqueue_script( $this->table_form_synergy, plugin_dir_url( __FILE__ ) . 'js/table-form-synergy-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->table_form_synergy . '-bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->table_form_synergy . '-toastr-js', 'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js', array( 'jquery' ), $this->version, false );

		// passo le variabiali a javascript
		wp_localize_script( $this->table_form_synergy, 'my_ajax_handler', $vars );

	}

	public function set_option_mail_synergy(): void
	{

		$this->mailer->isSMTP();
		$this->mailer->Host = 'sandbox.smtp.mailtrap.io';
		$this->mailer->SMTPAuth = true;
		$this->mailer->Port = 2525;
		$this->mailer->Username = 'ded9abe0e55e8f';
		$this->mailer->Password = '96e7209d071fd4';

	}

	public function set_hook_option_mail_synergy( $mailer ): void
	{
		$mailer->isSMTP();
		$mailer->Host = 'sandbox.smtp.mailtrap.io';
		$mailer->SMTPAuth = true;
		$mailer->Port = 2525;
		$mailer->Username = 'ded9abe0e55e8f';
		$mailer->Password = '96e7209d071fd4';
	}

	public function get_from_email_synergy(  ) {
		return $this->EMAIL_FROM;
	}

	public function store_form_information(): void
	{
		$nonce = $_POST['nonce'];

		if( !wp_verify_nonce($nonce, 'my-ajax-handler-nonce') )
			wp_send_json_error('Nonce error');

		$name = sanitize_text_field($_POST['name']);
		$surname = sanitize_text_field($_POST['surname']);
		$birth = sanitize_text_field($_POST['birth']);
		$born = sanitize_text_field($_POST['born']);
		$email = sanitize_email($_POST['email']);

		try{
			global $wpdb;
			$table = $wpdb->prefix . 'table_form_synergy';
			$wpdb->insert( $table, [
				"id" => NULL, //AUTO-GENERATED
				"name" => $name,
				"surname" => $surname,
				"email" => $email,
				"date_of_birth" => $birth,
				"born_in" => $born,
			]);

			if(!empty($wpdb->last_error)){
				wp_send_json_error(["error" => $wpdb->last_error]);
			}

//			$args = [
//				"email" => $email,
//				"name" => $name,
//				"body" => 'Sono interessato a un tuo colloquio.',
//			];

			/**
			 * string|string[] $to, string $subject, string $message, string|string[] $headers = '', string|string[] $attachments = array()
			 */
			//$message_email = $this->send_mail_synergy($this->mailer, $args);

			$res = wp_mail($email,'Oggetto prova','Messaggio di test','','');

			wp_send_json_success(["message"=>'Inserito correttamente', "email"=>$res]);


		}catch (Exception $exception){
			wp_send_json_error(["error" =>$exception]);
		}

	}

}

function render_shortcode_table_form_synergy() : string|bool
{
    ob_start();
    include(__DIR__ . '/partials/table-form-synergy-public-display.php');
    return ob_get_clean();
}

add_shortcode('render_table_form_synergy','render_shortcode_table_form_synergy');


//add_action( 'phpmailer_init', 'set_hook_option_mail_synergy' );
