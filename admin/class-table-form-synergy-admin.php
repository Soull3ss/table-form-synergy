<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Table_Form_Synergy
 * @subpackage Table_Form_Synergy/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Table_Form_Synergy
 * @subpackage Table_Form_Synergy/admin
 * @author     Your Name <email@example.com>
 */
class Table_Form_Synergy_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $table_form_synergy    The ID of this plugin.
	 */
	private string $table_form_synergy;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private string $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $table_form_synergy       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $table_form_synergy, $version )
    {

		$this->table_form_synergy = $table_form_synergy;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() : void
    {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in table_form_synergy_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The table_form_synergy_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->table_form_synergy, plugin_dir_url(__FILE__) . 'css/table-form-synergy-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->table_form_synergy. '-bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->table_form_synergy. '-datatable-css', 'https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css', array(), $this->version, 'all' );



	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() : void
    {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in table_form_synergy_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The table_form_synergy_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->table_form_synergy, plugin_dir_url(__FILE__) . 'js/table-form-synergy-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->table_form_synergy . '-bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->table_form_synergy . '-datatable-js', 'https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js', array( 'jquery' ), $this->version, false );

	}
    /**
     * Register a custom menu page.
     */
    public function create_menu_admin() : void
    {
        add_menu_page(
            __( 'Table Form Synergy', 'form-table-synergy' ),
            'Table Form Synergy',
            'manage_options',
            'dashboard-menu-synergy',
            [$this,'load_dashboard'],
            null,
            6
        );
        add_submenu_page(
            'dashboard-menu-synergy',
            __('Contattaci','form-table-synergy'),
            __('Contattaci','form-table-synergy'),
            'manage_options',
            'wp-diet-customers',
            [$this, 'load_contacts']
        );
    }

    public function load_dashboard() : void
    {
//        new Wp_Diet_Admin_Dashboard($this->plugin_name, $this->version);
        global $wpdb;
        $rows = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}table_form_synergy", ARRAY_A );

        require_once (__DIR__ . '/partials/table-form-synergy-admin-display.php');
    }

    public function load_contacts() : void
    {
        //todo
    }

}
