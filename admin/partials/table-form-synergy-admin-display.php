<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    table_form_synergy
 * @subpackage table_form_synergy/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="container mt-5">
        <table class="table table-hover" id="table-admin-form-synergy">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Cognome</th>
                    <th scope="col">Email</th>
                    <th scope="col">Data di nascita</th>
                    <th scope="col">Nato a</th>
                    <th scope="col">Inserito</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($rows as $row) : ?>
                    <tr>
                        <th scope="row"><?=$row['id']?></th>
                        <td><?=$row['name']?></td>
                        <td><?=$row['surname']?></td>
                        <td><?=$row['email']?></td>
                        <td><?=$row['date_of_birth']?></td>
                        <td><?=$row['born_in']?></td>
                        <td><?=$row['submitted_at']?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
</div>