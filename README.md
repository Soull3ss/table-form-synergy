# Table Form Synergy

## Descrizione 
Questo è un semplice plugin per aiutare le persone a iniziare lo sviluppo in Wordpress
avendo un boiler di base e alcune funzionalità già sviluppate utilizzabili per creare
dei plugin personali.
Nel plugin è compresa la gestione con una chiamata ajax partendo dal front end
e una piccola gestione mediante menù di una tabella che preleva dati dal database.

## Installazione
Per l'installazione si può sia scarica lo zip e quindi installare direttamente in Wordpress.
Oppure si può clone il git nella cartella wp-conten/plugins/.

## License

For open source projects, say how it is licensed.

## Supporto

Url : https://ufficiosomma.com/